module.exports = {
    "env": {
        "jquery": true,
        "browser": true,
        "es6": true
    },
    "extends": "airbnb-base",
    "globals": {
        "_": false,
        "gl": false,
        "gon": false,
        "localStorage": false
    },
    "rules":{
        
    },
    "plugins": [
        "import"
    ]
};
